import asyncio
import json
import subprocess
import threading
import time
from time import sleep
import websocket
import GameStatus

booting = True

# Various JSON constants we need:
ping = '{"action": "Ping"}'
# trip_points = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(1).TripScore","value":4,"flag":""}'
# trip_add = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(1).AddTrip","value":true,"flag":""}'
# lead_awarded = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(1).Lead","value":true,"flag":""}'
# lead_lost = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(1).Lost","value":true,"flag":""}'
# star_pass = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(2).StarPass","value":true,"flag":""}'
registration = {
    "action": "Register",
    "paths": [
        "ScoreBoard.CurrentGame.InJam",
    ]
}
# we want Jam Is On / Jam is Off, and the names of the colours for each team.

sb_connection: websocket.WebSocketApp
sb_ignore = True


def on_error(ws, error):
    print('!!! ERROR:', error)


def on_close(ws):
    print("### closed ###")


def send(message) -> None:
    class Sender(threading.Thread):
        def __init__(self, ws, message_text):
            threading.Thread.__init__(self)
            self.ws = ws
            self.message_text = message_text

        def run(self) -> None:
            self.ws.send(self.message_text)

    global sb_connection
    message_text = json.dumps(message)
    print(message_text)
    Sender(sb_connection, message_text).start()
    return


def jam_start() -> None:
    send({
        "action": "Set",
        "key": f"ScoreBoard.CurrentGame.StartJam",
        "value": True,
        "flag": ""
    })


def jam_end() -> None:
    send({
        "action": "Set",
        "key": f"ScoreBoard.CurrentGame.StopJam",
        "value": True,
        "flag": ""
    })


def official_timeout() -> None:
    send({
        "action": "Set",
        "key": "ScoreBoard.CurrentGame.OfficialTimeout",
        "value": True,
        "flag": ""
    })


def tto(team) -> None:
    send({
        "action": "Set",
        "key": f"ScoreBoard.CurrentGame.Team({int(team)}).Timeout",
        "value": True,
        "flag": ""
    })


def official_review(team) -> None:
    send({
        "action": "Set",
        "key": f"ScoreBoard.CurrentGame.Team({int(team)}).OfficialReview",
        "value": True,
        "flag": ""
    })


def on_open(ws):
    class KeepAlive(threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)

        def run(self):
            while True:
                sleep(25)
                ws.send(ping)

    ws.send(json.dumps(registration))
    KeepAlive().start()


def on_message(ws, message):
    print('Message:', message)
    response = json.loads(message)
    if 'state' not in response:
        return
    state = response['state']
    if 'WS.Client.Id' in state:
        return
    print(state)
    for key in state:
        print(key)
        if key == "ScoreBoard.CurrentGame.Team(1).Name" and GameStatus.team_number == 1:
            colour = state[key].lower()
            print('Colour is', colour)
            if colour in GameStatus.colour_sounds:
                GameStatus.colour_wav = GameStatus.colour_sounds[colour]
        if key == "ScoreBoard.CurrentGame.Team(2).Name" and GameStatus.team_number == 2:
            colour = state[key].lower()
            print('Colour is', colour)
            if colour in GameStatus.colour_sounds:
                GameStatus.colour_wav = GameStatus.colour_sounds[colour]
        if key == 'ScoreBoard.CurrentGame.InJam':
            # Note well: this can be used by P5 to ignore the right hand accelerometer
            if state[key]:
                print('Jam is on')
                if not GameStatus.jam_on:
                    # edge detection -> set initial trip to true
                    print('New Jam')
            else:
                print('Jam is off')
            GameStatus.jam_on = state[key]
        if key in ['ScoreBoard.CurrentGame.Team(1).DisplayLead', 'ScoreBoard.CurrentGame.Team(2).DisplayLead']:
            team = int(key.split('(')[1].split(')')[0])
            if not state[key]:
                print('Scoreboard: Not lead')
                # for some reason the scoreboard is confirming a "no"
                continue
            print(f"Team {team} is lead. We are {GameStatus.team_number}")
            if GameStatus.team_number != team:
                print('\tWhich is not us')
                GameStatus.lead_currently = False
                GameStatus.lead_us = False
                GameStatus.lead_available = False
            else:
                print('\tWhich is us')
                GameStatus.lead_currently = True
                GameStatus.lead_us = True
                GameStatus.lead_available = False
                GameStatus.initial_trip = False
            # {"state": {"ScoreBoard.CurrentGame.Team(1).DisplayLead": true}}


def scoreboard_start(ws_url):
    # websocket.enableTrace(True)
    class ScoreboardRunner(threading.Thread):
        def __init__(self, ws_url):
            threading.Thread.__init__(self)
            self.ws_url = ws_url

        def run(self):
            global sb_connection
            sb_connection = websocket.WebSocketApp(self.ws_url,
                                                   on_message=on_message,
                                                   on_error=on_error,
                                                   on_close=on_close)
            sb_connection.on_open = on_open
            sb_connection.run_forever()

    ScoreboardRunner(ws_url).start()


def scoreboard_stop():
    global sb_connection
    if sb_connection:
        sb_connection.close()
