import getopt
import subprocess
import sys
from time import time
from tkinter import messagebox

import GameStatus
import Scoreboard
from ScoringGauntlet import ScoringGauntlet

# Prototype of automated score keeper.
# 2021-02-10: Removing accelerometer and replace with "send", "lead" and "lead lost" buttons

args = {}

DEBUG_INTERVAL = 2

#    Right bits:
#    0: star pass
#    1: new pass / "send"
#    2: calloff
#    3: Star removed / ineligible for lead
#    5: lead declared (this button tracks when the finger is down, not up)

# We are turning the right hand gauntlet into a chording keyboard:
rindex_start = 0
rindex_end = 1
rindex_or = 2
rindex_tto = 3

# these make sense if rindex_or is held down
rindex_or_1 = 0
rindex_or_2 = 1

# these make sense if rindex_tto is held down
rindex_tto_1 = 0
rindex_tto_2 = 1

right_active = '0'
# in case we change our minds, but we're not performing inversion on the right hand set of buttons
# anymore
speaking_process = None


def gauntlet_loop(gauntlet):
    global speaking_process
    if gauntlet.connect():
        messagebox.showinfo("Connected", "Connected To Gauntlet")
    else:
        messagebox.showwarning("Unable To Connect", "Unable To Connect To Gauntlet")
        exit(1)

    # While the buttons track different states (eg. left hand buttons are "1" when the switch is not pressed, but
    # the right hand buttons are 1 if the buttons ARE pressed) they both mean the same things - 1 means "on"
    last_click = {
        'review': 0,
        'tto': 0
    }
    # this only needs to stop us from paying attention to the call off button from just after the first time it is
    # pressed until when the scoreboard broadcasts that the jam is off.

    while gauntlet.keepRunning:
        line = gauntlet.readln().strip()
        fields = line.replace('  ', ' ').split(' ')

        if len(fields) < 2 or fields[0][0] == '#':
            continue

        right_hand_buttons = fields[1]
        right_hand_buttons = right_hand_buttons[::-1]
        while len(right_hand_buttons) < 8:
            right_hand_buttons = f'{right_hand_buttons}0'

        if not GameStatus.jam_on:
            if right_hand_buttons[rindex_tto] == right_active and right_hand_buttons[rindex_or] == right_active:
                last_click['tto'] = time()
                Scoreboard.official_timeout()
                continue
            elif right_hand_buttons[rindex_or] == right_active:
                if time() - last_click['review'] > DEBUG_INTERVAL:
                    if right_hand_buttons[rindex_or_1] == right_active:
                        last_click['review'] = time()
                        Scoreboard.official_review(1)
                    elif right_hand_buttons[rindex_or_2] == right_active:
                        last_click['review'] = time()
                        Scoreboard.official_review(2)
                continue
            elif right_hand_buttons[rindex_tto] == right_active:
                if time() - last_click['tto'] > DEBUG_INTERVAL:
                    if right_hand_buttons[rindex_tto_1] == right_active:
                        last_click['tto'] = time()
                        Scoreboard.tto(1)
                    elif right_hand_buttons[rindex_tto_2] == right_active:
                        last_click['tto'] = time()
                        Scoreboard.tto(2)
                continue

        if not GameStatus.jam_on:
            if right_hand_buttons[rindex_start] == right_active:
                Scoreboard.jam_start()
                print('\tStarting')

        if right_hand_buttons[rindex_end] == right_active:
            Scoreboard.jam_end()
            print('\tCalling / stopping')


if __name__ == '__main__':
    # Command-line arguments are:
    # g: hostname (of Gauntlet) - assumes port 23 ala ESP-01S esp-link.
    # b: scoreboard URL eg. ws://localhost:8000/WS/
    # t: team number in CRG-ese (i.e. 1 or 2)
    opt_list, arg_list = getopt.getopt(sys.argv[1:], "d:g:b:")
    # print(opt_list)
    for item in opt_list:
        key = item[0].replace("-", "")
        for the_arg in item[1:]:
            if key in args:
                args[key].append(the_arg)
            else:
                args[key] = [the_arg]
    GameStatus.sound_path = './'
    if 'd' in args:
        GameStatus.sound_path = args['d'][0]
    if not GameStatus.sound_path.endswith('/'):
        GameStatus.sound_path += '/'

    if 'g' not in args:
        args['g'] = ['192.168.0.41']
    if 'b' in args:
        Scoreboard.scoreboard_start(args['b'][0])
    else:
        print('Not enough scoreboard connection information')
        exit(1)

    gauntlet_loop(ScoringGauntlet(args['g'][0], 23))
    exit(0)
