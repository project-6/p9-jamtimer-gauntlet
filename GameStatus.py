# A collection of random things so that we can determine what is happening from our perspective

jam_on = False
initial_trip = False

# Is lead available? As in, available to "me"?
lead_available = False

# Are we lead?
lead_currently = False

# Are we in a position to messing around with lead?
lead_us = False

# We need to track this given the JR will skate around with their hand down
lead_lost_reported = False

team_number = None

colour_sounds = {
    'black': '09-Black.wav',
    'white': '10-White.wav',
    'green': '11-Green.wav',
    'gold': '12-Gold.wav',
    'red': '13-Red.wav',
    'blue': '14-Blue.wav',
    'yellow': '15-Yellow.wav',
    'maroon': '16-Maroon.wav',
    'pink': '17-Pink.wav',
    'purple': '18-Purple.wav',
    'celeste': '19-Celeste.wav',
    'orange': '20-Orange.wav',
    'brown': '21-Brown.wav',
    'teal': '22-Teal.wav'
}

colour_wav = ''

sound_path = ''
